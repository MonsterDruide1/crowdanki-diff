import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { MatDialogRef } from '@angular/material/dialog';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-authentication-modal',
  templateUrl: './authentication-modal.component.html',
  styleUrls: ['./authentication-modal.component.scss']
})
export class AuthenticationModalComponent implements OnInit {
  authForm = new UntypedFormGroup({
    authToken: new UntypedFormControl("", [
      Validators.required,
    ])
  });

  constructor(private router: Router, private matDialogReference: MatDialogRef<AuthenticationModalComponent>) { }

  ngOnInit() {
  }

  onSubmit(): void {
    this.authForm.markAsDirty();
    if(this.authForm.valid) {
      this.matDialogReference.close(this.authForm.get("authToken").value);
    }
  }

  onCancelClick() {
    this.router.navigateByUrl('/home');
  }

}
