import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HeaderComponent} from './header/header.component';
import {MaterialModule} from '../material/material.module';
import {RouterModule} from '@angular/router';
import { DiffComponent } from './diff/diff.component';
import { AuthenticationModalComponent } from './authentication-modal/authentication-modal.component';
import {FormsModule} from '@angular/forms';
import { NotePreviewComponent } from './note-preview/note-preview.component';
import { FieldPreviewComponent } from './field-preview/field-preview.component';
import {AppModule} from '../app.module';
import {PreprocessFieldPipe} from '../pipes/preprocess-field.pipe';
import {MathJaxModule} from 'ngx-mathjax';
import {KeepHtmlPipe} from '../pipes/keep-html.pipe';
import { NoteSelectorComponent } from './note-selector/note-selector.component';
import { NoteSelectorItemComponent } from './note-selector-item/note-selector-item.component';
import {ChangedCardsOnlyPipe} from '../pipes/changed-cards-only.pipe';
import {BeautifyHtmlPipe} from '../pipes/beautify-html.pipe';
import { ReactiveFormsModule } from '@angular/forms';
import {HIGHLIGHT_OPTIONS, HighlightModule} from 'ngx-highlightjs';

export function getHighlightLanguages() {
  return {
    xml: () => import('highlight.js/lib/languages/xml'),
  };
}

@NgModule({
    declarations: [
        AuthenticationModalComponent,
        BeautifyHtmlPipe,
        ChangedCardsOnlyPipe,
        DiffComponent,
        FieldPreviewComponent,
        HeaderComponent,
        KeepHtmlPipe,
        NotePreviewComponent,
        NoteSelectorComponent,
        NoteSelectorItemComponent,
        PreprocessFieldPipe,
    ],
    imports: [
        CommonModule,
        FormsModule,
        HighlightModule,
        MaterialModule,
        MathJaxModule.forChild(),
        ReactiveFormsModule,
        RouterModule,
    ],
    exports: [
        BeautifyHtmlPipe,
        ChangedCardsOnlyPipe,
        HeaderComponent,
        KeepHtmlPipe,
        NoteSelectorComponent,
        NoteSelectorItemComponent,
        PreprocessFieldPipe,
    ],
    providers: [
			{
					provide: HIGHLIGHT_OPTIONS,
					useValue: {
							coreLibraryLoader: () => import('highlight.js/lib/core'),
							lineNumbersLoader: () => import('highlightjs-line-numbers.js'),
							languages: getHighlightLanguages(),
							themePath: 'assets/hljs-themes/a11y-dark.css',
					}
			}
    ]
})
export class ComponentsModule { }
