import {Component, Input, OnChanges, OnInit, ViewChild} from '@angular/core';
import {MathJaxDirective} from 'ngx-mathjax';

@Component({
  selector: 'app-field-preview',
  templateUrl: './field-preview.component.html',
  styleUrls: ['./field-preview.component.scss']
})
export class FieldPreviewComponent implements OnInit, OnChanges {
  @Input() field: string;
  @ViewChild('math', { static: false, read: MathJaxDirective })
  mathJax?: MathJaxDirective;
  fieldContent = '';
  @Input() commitId: string;
  @Input() deckPath: string;

  ngOnInit(): void {
  }

  ngOnChanges() {
    this.fieldContent = this.field;
    setTimeout(() => {
      this.mathJax.MathJaxTypeset();
    });
  }
}
