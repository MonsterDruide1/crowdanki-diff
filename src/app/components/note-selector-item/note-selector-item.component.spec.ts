import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NoteSelectorItemComponent } from './note-selector-item.component';

describe('NoteSelectorItemComponent', () => {
  let component: NoteSelectorItemComponent;
  let fixture: ComponentFixture<NoteSelectorItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoteSelectorItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoteSelectorItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
