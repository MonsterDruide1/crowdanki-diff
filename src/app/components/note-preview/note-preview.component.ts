import {Component, Input, OnInit} from '@angular/core';
import {Note} from '../../models/note';

@Component({
  selector: 'app-note-preview',
  templateUrl: './note-preview.component.html',
  styleUrls: ['./note-preview.component.scss']
})
export class NotePreviewComponent {
  @Input() note: Note;
  showingSource = false;
  @Input() title = 'Card';
  @Input() commitId: string;
  @Input() deckPath: string;

  constructor() { }

  toggleSourcePreview(): void {
    this.showingSource = !this.showingSource;
  }
}
