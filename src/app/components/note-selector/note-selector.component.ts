import {Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild} from '@angular/core';
import {Deck} from '../../models/deck';
import {DeckService} from '../../services/deck.service';
import {DeckDiff} from '../../models/deck-diff';
import {NoteDiff} from '../../models/note-diff';
import {NoteSelectorItemComponent} from '../note-selector-item/note-selector-item.component';

@Component({
  selector: 'app-note-selector',
  templateUrl: './note-selector.component.html',
  styleUrls: ['./note-selector.component.scss']
})
export class NoteSelectorComponent implements OnChanges {
  @Input() leftDeck: Deck;
  @Input() rightDeck: Deck;
  @Input() hideCardsWithoutChanges: boolean;
  deckDiff: DeckDiff;
  @ViewChild(NoteSelectorItemComponent, {static: false}) items: NoteSelectorItemComponent;
  @Output() newSelection: EventEmitter<NoteDiff> = new EventEmitter<NoteDiff>();
  selectedNoteDiff: NoteDiff;

  constructor(
    private deckService: DeckService
  ) {
  }

  ngOnChanges() {
    if (this.leftDeck && this.rightDeck) {
      this.refreshDiff();
    }
  }

  refreshDiff() {
    if (this.leftDeck && this.rightDeck) {
      this.deckDiff = this.deckService.diffDecks(this.leftDeck, this.rightDeck);
    }
  }

  select(ev: Event, noteDiff: NoteDiff) {
    this.newSelection.emit(noteDiff);
    this.selectedNoteDiff = noteDiff;
  }
}
