import {Injectable} from '@angular/core';
import {Gitlab, MergeRequests, ProjectSchema} from 'gitlab';
import {CookieService} from 'ngx-cookie-service';
import {MergeRequest} from '../models/merge-request';
import {Branch} from '../models/branch';

@Injectable({
  providedIn: 'root'
})
export class GitlabService {
  static cachedProjectId: number;
  static cachedProject: ProjectSchema;
  static cachedMergeRequestId: number;
  static cachedMergeRequest: MergeRequest;
  static authenticated = false;

  constructor(private cookieService: CookieService) {
  }

  static api = new Gitlab();

  authenticate(token: string) {
    GitlabService.api = new Gitlab({
      token
    });
    const expirationDate = new Date();
    expirationDate.setDate(expirationDate.getDate() + 180);
    this.cookieService.set('gitlab-auth-token', token, expirationDate, '/');
    GitlabService.authenticated = true;
  }

  authenticateWithCookie() {
    if (this.cookieService.check('gitlab-auth-token')) {
      this.authenticate(this.cookieService.get('gitlab-auth-token'));
      return true;
    } else {
      return false;
    }
  }

  async getProjectUrl(projectId: number) {
    if (projectId !== GitlabService.cachedProjectId) {
      GitlabService.cachedProject = await GitlabService.api.Projects.show(projectId) as ProjectSchema;
      GitlabService.cachedProjectId = projectId;
    }
    return GitlabService.cachedProject.http_url_to_repo;
  }

  async getProjectName(projectId: number) {
    if (projectId !== GitlabService.cachedProjectId) {
      GitlabService.cachedProject = await GitlabService.api.Projects.show(projectId) as ProjectSchema;
      GitlabService.cachedProjectId = projectId;
    }
    return GitlabService.cachedProject.name;
  }

  async getMergeRequestName(projectId: number, mergeRequestId: number) {
    if (projectId !== GitlabService.cachedProjectId || mergeRequestId !== GitlabService.cachedMergeRequestId) {
      GitlabService.cachedProject = await GitlabService.api.Projects.show(projectId) as ProjectSchema;
      GitlabService.cachedProjectId = projectId;
      GitlabService.cachedMergeRequest = await GitlabService.api.MergeRequests.show(projectId, mergeRequestId) as MergeRequest;
      GitlabService.cachedMergeRequestId = mergeRequestId;
    }
    return GitlabService.cachedMergeRequest.title;
  }

  async getMergeRequestUrl(projectId: number, mergeRequestId: number) {
    if (projectId !== GitlabService.cachedProjectId || mergeRequestId !== GitlabService.cachedMergeRequestId) {
      GitlabService.cachedProject = await GitlabService.api.Projects.show(projectId) as ProjectSchema;
      GitlabService.cachedProjectId = projectId;
      GitlabService.cachedMergeRequest = await GitlabService.api.MergeRequests.show(projectId, mergeRequestId) as MergeRequest;
      GitlabService.cachedMergeRequestId = mergeRequestId;
    }
    return GitlabService.cachedMergeRequest.web_url;
  }

  async getIncomingCommitId(projectId: number, mergeRequestId) {
    const commits = await GitlabService.api.MergeRequests.commits(projectId, mergeRequestId);
    return commits[0].id;
  }

  async getIncomingDeckNames(projectId: number, mergeRequestId: number) {
    // TODO : Handle request error
    const files = await GitlabService.api.Repositories.tree(projectId, {
      recursive: true
    }) as Array<any>;

    const names = [];
    files.forEach(file => {
      if (file.name === 'deck.json') {
        names.push(file.path.substring(0, file.path.length - 10));
      }
    });
    return names;
  }

  async getIncomingDeck(projectId: number, mergeRequestId: number, path: string) {
    const ref = await this.getIncomingCommitId(projectId, mergeRequestId);
    return GitlabService.api.RepositoryFiles.showRaw(projectId, `${path}/deck.json`, ref);
  }

  async getCurrentCommitId(projectId: number, mergeRequestId) {
    const mr = await GitlabService.api.MergeRequests.show(projectId, mergeRequestId) as MergeRequest;
    const branch = await GitlabService.api.Branches.show(mr.target_project_id, mr.target_branch) as Branch;
    return branch.commit.id;
  }

  async getCurrentDeckNames(projectId: number, mergeRequestId: number) {
    // TODO : Handle request error
    const files = await GitlabService.api.Repositories.tree(projectId, {
      recursive: true
    }) as Array<any>;

    const names = [];
    files.forEach(file => {
      if (file.name === 'deck.json') {
        names.push(file.path.substring(0, file.path.length - 10));
      }
    });
    return names;
  }

  async getCurrentDeck(projectId: number, mergeRequestId: number, path: string) {
    const ref = await this.getCurrentCommitId(projectId, mergeRequestId);
    return GitlabService.api.RepositoryFiles.showRaw(projectId, `${path}/deck.json`, ref);
  }
}
