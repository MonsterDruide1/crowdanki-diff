import {Injectable} from '@angular/core';
import {Note} from '../models/note';
import {NoteDiff} from '../models/note-diff';

@Injectable({
  providedIn: 'root'
})
export class NoteService {

  constructor() {
  }

  diffNotes(firstNote: Note, secondNote: Note): NoteDiff {
    let changes = false;
    const changesInFields: boolean[] = [];
    let changesInTags = false;
    let noteGuid = null;

    if (firstNote && secondNote) {
      firstNote.fields.forEach((firstField, idx) => {
        changesInFields[idx] = (firstField !== secondNote.fields[idx]);
        if (changesInFields[idx]) {
          changes = true;
        }
      });

      changesInTags = (firstNote.tags !== secondNote.tags);
      noteGuid = firstNote.guid;
    } else if (firstNote) {
      firstNote.fields.forEach((field, idx) => {
        changesInFields[idx] = true;
        changes = true;
      });
      changesInTags = true;
      noteGuid = firstNote.guid;
    } else if (secondNote) {
      secondNote.fields.forEach((field, idx) => {
        changesInFields[idx] = true;
        changes = true;
      });
      changesInTags = true;
      noteGuid = secondNote.guid;
    }

    return {
      firstExists: !!firstNote,
      secondExists: !!secondNote,
      changesInFields,
      changes,
      changesInTags,
      noteGuid
    };
  }
}
