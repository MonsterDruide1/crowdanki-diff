import { Pipe, PipeTransform } from '@angular/core';
import {NoteDiff} from '../models/note-diff';

@Pipe({
  name: 'changedCardsOnly'
})
export class ChangedCardsOnlyPipe implements PipeTransform {

  transform(value: NoteDiff[], enable: boolean = true): NoteDiff[] {
    if (value === null) {
      value = [];
    }
    if (enable) {
      return value.filter((note: NoteDiff) => note.changes);
    } else {
      return value;
    }
  }

}
