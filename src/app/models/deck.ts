import {Note} from './note';

export interface Deck {
  children: [];
  crowdanki_uuid: string;
  deck_config_uuid: string;
  deck_configurations: [];
  desc: string;
  dyn: number;
  extendNew: number;
  extendRev: number;
  media_files: string[];
  name: string;
  note_models: string[];
  notes: Note[];
}
