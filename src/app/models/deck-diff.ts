import {NoteDiff} from './note-diff';

export interface DeckDiff {
  noteDiffs: NoteDiff[];
}
