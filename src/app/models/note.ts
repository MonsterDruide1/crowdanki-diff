export interface Note {
  data: string;
  fields: Array<string>;
  flags: number;
  guid: string;
  note_model_uuid: string;
  tags: string[];
}
