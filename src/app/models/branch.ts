import {Commit} from './commit';

export interface Branch {
  name: string;
  commit: Commit;
    // TODO : Add missing stuff
}
