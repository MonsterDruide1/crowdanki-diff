export interface NoteDiff {
  noteGuid: string;
  changes: boolean;
  firstExists: boolean;
  secondExists: boolean;
  changesInFields: boolean[];
  changesInTags: boolean;
}
